# Overview
`f-scrolltop`组件封装了页面滚动至顶部的功能

# Usage

在页面中加入： 

```
// develop
<link rel="stylesheet" href="../dist/style.css">
<script src="../dist/FUI.js"></script>

// production
<link rel="stylesheet" href="../dist/style.min.css">
<script src="../dist/FUI.min.js"></script>
```

首先需要确保页面中有引入```vue.js```。所有的组件存在于全局变量```FUI```中。

注册组件：

```Javascript
Vue.component('scrolltop', FUI.scrolltop)
```

然后使用标签：

```
<scrolltop :threshold="100"></scrolltop>
```


# API
| Option             | Description                                            | Value   | Default  |
|--------------------|--------------------------------------------------------|---------|----------|
| threshold          | 整数，表示页面滚动距离等于threshold的值时候，开始出现置顶按钮   | Number  |   0      |

# events
| 事件名             | 描述                              |
|-------------------|----------------------------------|
| scroll-show       | 置顶按钮出现的时候触发               |
| scroll-hide       | 置顶按钮隐藏的时候触发               |
| scroll-end         | 页面滚动顶部的时候触发               |

# example
```
./example/scrolltop.html
```

# 支持Vue 2

# License
MIT