# Overview
`f-loading`组件用于加载数据的时候，显示加载动画，多用于列表加载

# Usage

在页面中加入： 

```
// develop
<link rel="stylesheet" href="../dist/style.css">
<script src="../dist/FUI.js"></script>

// production
<link rel="stylesheet" href="../dist/style.min.css">
<script src="../dist/FUI.min.js"></script>
```

首先需要确保页面中有引入```vue.js```。所有的组件存在于全局变量```FUI```中。

注册组件：

```Javascript
Vue.component('f-loading', FUI.loading)
```

然后使用标签：

```
<f-loading :visible="visible"></f-loading>
```


# API
| Option             | Description                 | Value   | Default  |
|--------------------|-----------------------------|---------|----------|
| visible            | 控制组件是否显示               | Boolean | false    |
| has-text           | 控制组件是否显示文字            | Boolean | true     |


# example
```
./example/loading.html
```

# License
MIT