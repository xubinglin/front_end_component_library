import Tween from './Tween.js'

const requestAnimFrame = (function () {
    return window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60)
        }
})()

export default {
    /**
     * 节流函数，用于降低某些方法的执行频率
     * @param  {Function} func  需要节流的函数
     * @param  {Number} delay 延迟时间，ms
     * @return {Function}
     */
    throttle(func, delay) {
        var timeout = null
        var lastRun = 0
        return function () {
            if (timeout) {
                return
            }
            var elapsed = +new Date() - lastRun
            var context = this
            var args = arguments
            var runCallback = function runCallback() {
                lastRun = +new Date()
                timeout = false
                func.apply(context, args)
            }

            if (elapsed >= delay) {
                runCallback()
            } else {
                timeout = setTimeout(runCallback, delay)
            }
        }
    },

    /**
     * 寻找某个元素的最近的父节点
     * @param  {HTMLElement} element   元素节点
     * @param  {String} className 类名
     * @return {HTMLElement}           元素节点
     */
    closest(element, className) {
        if (!className) return element

        if (element.classList.contains(className)) return element
        if (element.closest) return element.closest('.' + className)

        while ((element = element.parentNode) && element && !element.classList.contains(className)) {

        }
        return element
    },

    /**
     * 判断是否为iPhone X
     */
    isiPhoneX() {
        return navigator.userAgent.match(/(iPhone)/) && window.screen.availHeight === 812 && window.screen.availWidth === 375
    },

    /**
     * 将HEX转化为RGBA
     * @param {String} hex hex颜色值
     * @param {Number} alpha 透明度，默认值：1
     */
    hex2rgba(hex, alpha = 1) {
        var c
        if (/^#([a-fA-F0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split('')
            if (c.length === 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]]
            }
            c = '0x' + c.join('')

            return {
                r: (c >> 16) & 255,
                g: (c >> 8) & 255,
                b: c & 255,
                a: alpha
            }
        }

        return null
    },

    isFunction(val) {
        return Object.prototype.toString.call(val) === '[object Function]'
    },

    isString(val) {
        return Object.prototype.toString.call(val) === '[object String]'
    },

    isObject(val) {
        return Object.prototype.toString.call(val) === '[object Object]'
    },

    isNumber(val) {
        return Object.prototype.toString.call(val) === '[object Number]'
    },

    /**
     * 对缓动算法的封装
     * @param {Number} from 起始数值
     * @param {Number} to 结束数值
     * @param {Number} duration 动画持续时间，单位：ms，默认：300
     * @param {String} easing 缓动类型，源自Tween，eg: 'Linear'，'Quad.easeIn'，'Bounce.easeInOut'，默认：'Linear'
     * @param {Function} callback 回调函数，支持2个参数（value, isEnding），其中value表示实时变化的计算值，isEnding是布尔值，表示动画是否完全停止
     */
    animation(from, to, duration = 300, easing = 'Linear', callback = (value, isEnding) => { }) {
        if (!this.isNumber(from) || !this.isNumber(to)) {
            if (window.console) {
                console.error('from和to两个参数需为数值')
            }
            return
        }

        var start = 0
        var self = this
        var toMilliSecond = function (val) {
            if (self.isNumber(val)) {
                return val
            }

            if (self.isString(val) && /^\d(\.\d+)+m?s$/i.test(val)) {
                if (/ms$/i.test(val)) {
                    return 1 * val.replace(/ms$/i, '')
                }

                return 1000 * val.replace(/s$/i, '')
            }

            if (self.isString(val) && /^\d+(\.\d+)$/.test(val)) {
                return parseInt(val, 10)
            }

            return -1
        }
        var during = Math.ceil(toMilliSecond(duration) / 17)
        var easingKey = easing.split('.')
        var easingFunction

        if (easingKey.length === 1) {
            easingFunction = Tween[easingKey[0]]
        } else if (easingKey.length === 2) {
            easingFunction = Tween[easingKey[0]] && Tween[easingKey[0]][easingKey[1]]
        }

        if (!this.isFunction(easingFunction)) {
            if (window.console) {
                console.error('没有找到名为"' + easing + '"的缓动函数')
            }
            return
        }

        var step = function () {
            var value = easingFunction(start, from, to, during)
            start++

            if (start <= during) {
                callback(value, false)
                requestAnimFrame(step)
            } else {
                callback(to, true)
            }
        }

        step()
    }
}
