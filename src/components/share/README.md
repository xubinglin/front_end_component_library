# Overview
`f-share`组件封装了分享按钮，支持朋友圈，微信好友，QQ好友和新浪微博

# Usage

在页面中加入： 

```
// develop
<link rel="stylesheet" href="../dist/style.css">
<script src="../dist/FUI.js"></script>

// production
<link rel="stylesheet" href="../dist/style.min.css">
<script src="../dist/FUI.min.js"></script>
```

首先需要确保页面中有引入```vue.js```。所有的组件存在于全局变量```FUI```中。

注册组件：

```Javascript
Vue.component('f-share', FUI.share)
```

然后使用标签：

```
<f-share
   :visible="visible"
   :share-item="shareItem"
   v-on:share="onShareHandle"></f-share>
```


# API
| Option             | Description                  | Value   | Default                               |
|--------------------|------------------------------|---------|---------------------------------------|
| visible            | 控制组件显示与隐藏              | Boolean | false                                 | 
| share-item         | 配置分享按钮                   | Array   | 默认包含了朋友圈，微信好友，QQ好友和新浪微博 |

`share-item`默认值：

```
[
   {image: '//cdn.upin.com/mfanhuan/images/faxian/ico_sharewx.png', text: '朋友圈', type: 'friend'},
   {image: '//cdn.upin.com/mfanhuan/images/faxian/ico_sharewxf.png', text: '微信好友', type: 'weixin'},
   {image: '//cdn.upin.com/mfanhuan/images/faxian/ico_shareqq.png', text: 'QQ好友', type: 'qq'},
   {image: '//cdn.upin.com/mfanhuan/images/faxian/ico_sharesina.png', text: '新浪微博', type: 'weibo'}
]

// image: icon图标地址
// text: 按钮文字
// type: 分享类别
```

# events
| 事件名             | 描述                   | 数据                         |
|-------------------|-----------------------|------------------------------|
| share             | 分享按钮被点击的时候触发  | `{type: "friend"}`           |
| share-cancle      | 分享取消的时候触发       |                              |

# example
```
./example/share.html
```

# License
MIT