# Overview
`f-good-list`商品列表模式视图

> 支持 Vue 2


# Usage

在页面中加入： 

```
// develop
<link rel="stylesheet" href="../dist/style.css">
<script src="../dist/FUI.js"></script>

// production
<link rel="stylesheet" href="../dist/style.min.css">
<script src="../dist/FUI.min.js"></script>
```

首先需要确保页面中有引入```vue.js```。所有的组件存在于全局变量```FUI```中。

注册组件：

```Javascript
// nav-flex
Vue.component('nav-flex', FUI.navFlex)

// nav-static
Vue.component('nav-static', FUI.navStatic)
```

然后使用标签：

```
// nav-flex
<nav-flex :list-item="listItem" ref:nav-flex></nav-flex>

// nav-static
<nav-static :list-item="listItem"></nav-static>
```


# API
| Option             | Description                                            | Value   | Default  |
|--------------------|--------------------------------------------------------|---------|----------|
| list-item          | 数组 定义导航的具体内容 必须属性                            | Array   |          |
| has-drop-down      | 布尔值 定义是否显示下拉导航 可选                            | Boolean | true     |

`list-item`范例：

```
listItem: [
    {text: '全部', type: 'all', id: '0', active: true},
    {text: '已返还', type: 'yf', id: '1', active: false},
    {text: '未返还', type: 'wf', id: '2', active: false},
    {text: '无返还', type: 'nullity', id: '3', active: false}
  ]
  
 // text: 内容
 // type: 类别
 // id: ID，必须独立唯一
 // active: 是否是选中状态
```


# events
| 事件名             | 描述                              |  数据        |
|-------------------|----------------------------------|--------------|
| nav-item-click      | 导航item被点击的时候触发            | 被点击项：`{text: '全部', type: 'all', id: '0', active: true}`|


# example
```
./example/nav.html
```

# methods
## getActiveItem()

获取当前导航的激活标签。
> 参数：无
> 
> 返回值：{Object} `{text: '全部', type: 'all', id: '0', active: true}`
> 
> 注意：使用这个方法必须索引到这个组件，建议使用`v-ref`，例如：`this.$refs.navFlex.getActiveItem()`

# License
MIT