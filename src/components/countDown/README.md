# Overview
`f-count-down`组件用于显示倒计时

# Usage

在页面中加入： 

```
// develop
<link rel="stylesheet" href="../dist/style.css">
<script src="../dist/FUI.js"></script>

// production
<link rel="stylesheet" href="../dist/style.min.css">
<script src="../dist/FUI.min.js"></script>
```

首先需要确保页面中有引入```vue.js```。所有的组件存在于全局变量```FUI```中。

注册组件：

```Javascript
Vue.component('f-count-down', FUI.countDown)
```

然后使用标签：

```
<f-count-down
  :left-count="5000"
  wrap-background="#ccc"
  span-background="red"></f-count-down>
```


# API
| Option             | Description                 | Value   | Default  |
|--------------------|-----------------------------|---------|----------|
| leftCount          | 剩余时间，单位：秒             | Number  | 0        |
| wrapBackground     | 设置组件background样式属性     | String  | '#ff3444'|
| spanBackground     | 设置数字单元background样式属性  | String  | '#000'   |


# events
| 事件名             | 描述                              |
|-------------------|----------------------------------|
| count-end         | 倒计时结束时候触发                  |


# example
```
./example/countDown.html
```

# License
MIT