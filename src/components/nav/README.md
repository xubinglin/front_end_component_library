# Overview
`f-nav-flex`导航列表，比如我的订单顶部的导航，宽度固定为容器宽度，各个导航项均分宽度

`f-nav-static`导航列表，比如分类页面顶部的导航，各个导航项宽度为各自内容宽度

`f-nav-image`图片风格导航列表

# Usage

在页面中加入： 

```
// develop
<link rel="stylesheet" href="../dist/style.css">
<script src="../dist/FUI.js"></script>

// production
<link rel="stylesheet" href="../dist/style.min.css">
<script src="../dist/FUI.min.js"></script>
```

首先需要确保页面中有引入```vue.js```。所有的组件存在于全局变量```FUI```中。

注册组件：

```Javascript
// nav-flex
Vue.component('f-nav-flex', FUI.navFlex)

// nav-static
Vue.component('f-nav-static', FUI.navStatic)

// nav-image
Vue.component('f-nav-image', FUI.navImage)
```

然后使用标签：

```
// nav-flex
<f-nav-flex :list-item="listItem" ref:nav-flex></f-nav-flex>

// nav-static
<f-nav-static :list-item="listItem"></f-nav-static>

// nav-image
<f-nav-image
  :list-item="listItem"
  :nav-config="navConfig"
  :category-bar="categoryBar"></f-nav-image>
```


# API

### For `<f-nav-flex/>`和`<f-nav-static/>`

| Option             | Description                                            | Value   | Default  |
|--------------------|--------------------------------------------------------|---------|----------|
| list-item          | 数组 定义导航的具体内容 必须                               | Array   |          |
| has-drop-down      | 布尔值 定义是否显示下拉导航 可选                            | Boolean | true     |

`list-item`范例：

```
listItem: [
    {text: '全部', type: 'all', id: '0', active: true},
    {text: '已返还', type: 'yf', id: '1', active: false},
    {text: '未返还', type: 'wf', id: '2', active: false},
    {text: '无返还', type: 'nullity', id: '3', active: false}
  ]
  
 // text: 内容
 // type: 类别
 // id: ID，必须独立唯一
 // active: 是否是选中状态
```

### For `<f-nav-image/>`

| Option             | Description                                            | Value   | Default  |
|--------------------|--------------------------------------------------------|---------|----------|
| list-item          | 数组 定义导航的具体内容 必须                               | Array   |          |
| nav-config         | 配置导航条背景色和前景色 必须                              | Object   |         |
| category-bar       | 配置导航条右侧入口 可选 如不配置，则不显示入口                | Object   |         |

`list-item`范例：

```
listItem: [
   {text: '精选', type: 'jiujiu_01', id: '0', active: false, imageUrl: 'https://image.fanhuan.com/common/img/20171127030205.png'},
   {text: '上新', type: 'jiujiu_10', id: '1', active: false, imageUrl: 'https://image.fanhuan.com/common/img/20171127050015.png'},
   {text: '服饰', type: 'jiujiu_02', id: '2', active: false, imageUrl: 'https://image.fanhuan.com/common/img/20171127030237.png'},
   {text: '家居', type: 'jiujiu_03', id: '3', active: false, imageUrl: 'https://image.fanhuan.com/common/img/20171127030255.png'},
   {text: '配饰', type: 'jiujiu_04', id: '4', active: false, imageUrl: 'https://image.fanhuan.com/common/img/20171127030334.png'}
  ]
  
 // text: 内容
 // type: 类别
 // id: ID，必须独立唯一
 // active: 是否是选中状态
 // imageUrl: 导航项背景图片
```

`nav-config`范例：

```
navConfig: {
	backGroundColor: '#2a323d',
	selectedBackGroundColor: ['#ff6646', '#fe1d61']
}

// backGroundColor: 导航条背景色
// selectedBackGroundColor: 选中导航项背景色值，第一个为渐变的起始值，第二个为渐变的终点值
```

`category-bar`范例：

```
categoryBar: {
	imageUrl: "http://image.fanhuan.com/mf/home_list_searcha_test20171121173736@3x.png?t=1511285856484.91",
	targetUrl: "http://m.fanhuan.com/home/catetory?a=1&usertype=1&paraname=token&enableTopSearchBar=blank",
}

// imageUrl: 入口背景图片
// targetUrl: 入口跳转链接
```



# events
| 事件名             | 描述                              |  数据        |  备注|
|-------------------|----------------------------------|--------------|------|
| nav-item-click    | 导航item被点击的时候触发             | 被点击项配置对象，参考listItem|     |
| category-bar-click| 右侧入口被点击的时候触发              | 被点击项配置对象，参考categoryBar | `<f-nav-image/>` only    |


# example
```
./example/nav.html
```

# License
MIT