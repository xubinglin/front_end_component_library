import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import scrolltop from '@/components/scrolltop/scrolltop.vue'
import navFlex from '@/components/nav/nav-flex.vue'
import navStatic from '@/components/nav/nav-static.vue'
import navImage from '@/components/nav/nav-image.vue'
import share from '@/components/share/share.vue'
import loading from '@/components/loading/loading.vue'
import goodsList from '@/components/goods/goods-list.vue'
import countDown from '@/components/countDown/count-down.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/scrolltop',
      name: 'scrolltop',
      component: scrolltop
    },
    {
      path: '/nav',
      name: 'navFlex',
      component: navFlex
    },
    {
      path: '/nav',
      name: 'navStatic',
      component: navStatic
    },
    {
      path: '/nav',
      name: 'navImage',
      component: navImage
    },
    {
      path: '/share',
      name: 'share',
      component: share
    },
    {
      path: '/loading',
      name: 'loading',
      component: loading
    },
    {
      path: '/goods',
      name: 'goods',
      component: goodsList
    },
    {
      path: '/countDown',
      name: 'countDown',
      component: countDown
    }    
  ]
})
