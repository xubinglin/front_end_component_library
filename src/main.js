// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
const scrolltop = require('@/components/scrolltop/scrolltop.vue')
const navFlex = require('@/components/nav/nav-flex.vue')
const navStatic = require('@/components/nav/nav-static.vue')
const navImage = require('@/components/nav/nav-image.vue')
const share = require('@/components/share/share.vue')
const loading = require('@/components/loading/loading.vue')
const goodsList = require('@/components/goods/goods-list.vue')
const countDown = require('@/components/countDown/count-down.vue')

const qems = {
  scrolltop,
  navFlex,
  navStatic,
  navImage,
  share,
  loading,
  goodsList,
  countDown
}

module.exports = qems
